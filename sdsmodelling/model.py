class Model:

    def __init__(self, name, version, fit_fn, predict_fn):
        self.name = name
        self.version = version
        self.__fit_fn = fit_fn
        self.__predict_fn = predict_fn
        self.params = None

    def fit(self, options, data):
        if self.__fit_fn:
            self.params = self.__fit_fn(options=options, data=data)
            return self.params
        else:
            return None

    def predict(self, options, data, params):
        if self.__predict_fn:
            return self.__predict_fn(options=options, data=data, params=params)
        else:
            return None
