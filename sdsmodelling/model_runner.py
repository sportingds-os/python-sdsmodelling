import datetime as dt
import time


class ModelRunner:

    def __init__(self, ds, model, options, get_runs_fn, get_fit_dataset_fn=None,
                 get_predict_dataset_fn=None, get_params_fn=None, write_fit_fn=None, write_predict_fn=None):
        self._ds = ds
        self._model = model
        self._options = options
        self.__get_runs_fn = get_runs_fn
        self.__get_fit_dataset_fn = get_fit_dataset_fn
        self.__get_predict_dataset_fn = get_predict_dataset_fn
        self.__get_params_fn = get_params_fn
        self.__write_fit_fn = write_fit_fn
        self.__write_predict_fn = write_predict_fn
        self._runs = []

    def get_runs(self):
        # This will return a list of objects, where each object has at least
        # identifier, type = {fit: bool, predict: bool}, options
        self._runs = self.__get_runs_fn()

        return self

    def _write_fit(self, run_identifier, params):
        if self.__write_fit_fn:
            # Next line makes the params generic for writing to model_parameters table
            params = self.__write_fit_fn(identifier=run_identifier, params=params)

        # TO DO: Write the code to write the generic parameters to model parameters table

    def _write_predict(self, run_identifier, predictions):
        if self.__write_predict_fn:
            # Next line makes the predictions generic for writing to model_predictions table
            predictions = self.__write_predict_fn(identifier=run_identifier, predictions=predictions)

        # TO DO: Write the code to write the generic predictions to model_predictions table

    def run(self):

        for item in self._runs:
            params = None
            if item['type']['fit']:
                mr_fit = ModelFitRun(self._model, item['identifier'], self.__get_fit_dataset_fn)
                params = mr_fit.run(item['options']['fit'])
                self._write_fit(item['identifier'], params)
            if item['type']['predict']:
                mr_predict = ModelPredictRun(self._model, item['identifier'], self.__get_predict_dataset_fn,
                                             self.__get_params_fn)
                predictions = mr_predict.run(item['options']['predict'], params)
                self._write_predict(item['identifier'], predictions)
        self._runs = []

    def monitor(self):
        if 'run' not in self._options or 'check_interval' not in self._options['run']:
            check_interval = 0
        else:
            check_interval = self._options['run']['check_interval']

        if check_interval is not None:
            last_check = False
            while True:
                if (not last_check) or last_check + dt.timedelta(seconds=check_interval) < dt.datetime.now():
                    self.get_runs().run()
                    last_check = dt.datetime.now()
                else:
                    time.sleep(1)

    def populate(self):

        self._options['dataset']['populate'] = True
        self.get_runs().run()


class ModelRun:

    def __init__(self, model, identifier, get_dataset_fn):
        self._model = model
        self._identifier = identifier
        self._get_dataset = get_dataset_fn
        self._data = None


class ModelFitRun(ModelRun):

    def run(self, options):
        data = self._get_dataset(identifier=self._identifier, fit_opts=options)
        return self._model.fit(options=options, data=data)


class ModelPredictRun(ModelRun):

    def __init__(self, model, identifier, get_dataset_fn, get_params_fn):
        super().__init__(model, identifier, get_dataset_fn)
        self._get_params = get_params_fn

    def run(self, options, params):
        data = self._get_dataset(identifier=self._identifier, predict_opts=options)
        if not params:
            params = self._get_params(identifier=self._identifier, predict_opts=options)

        return self._model.predict(options=options, data=data, params=params)
