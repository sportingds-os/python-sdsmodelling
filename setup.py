import setuptools as sut

sut.setup(
    name="sdsmodelling",
    version="0.1.0",
    packages=sut.find_packages(),

    author="DaveHastie",
    author_email="dave.hastie@sportingds.com",
    description="Package for Sporting Data Science modelling",
    license="FILE",
    include_package_data=True
)
